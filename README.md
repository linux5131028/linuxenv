# linuxenv

## aliases

```bash
for f in alias_*; do ln -s $(pwd)/$f ~/.$f; done
# In ~/.bashrc
for f in ~/.alias_*; do test -L $f && . $f || true; done
```

```bash
kubectl run k6-test --image=grafana/k6  --overrides='{"spec":{"restartPolicy": "Never","containers":[{"name":"k6","command":["sleep","3600000"],"image":"grafana/k6","resources":{"limits":{"cpu":"900m","memory":"1Gi"},"requests":{"cpu":"500m","memory":"50Mi"}}}]}}'
k cp ca001.js k6-test:/home/k6
kubectl exec --tty -i k6-test -- sh
# k delete pod k6-test
```


```bash

# tests:
Defaulting debug container name to debugger-cxx54.
Warning: container debugger-cxx54: Container image "lightruncom/koolkits:jvm" is not present with pull policy of Never
  ephemeralContainerStatuses:
  - image: lightruncom/koolkits:jvm
    imageID: ""
    lastState: {}
    name: debugger-cxx54
    ready: false
    restartCount: 0
    state:
      waiting:
        message: Container image "lightruncom/koolkits:jvm" is not present with pull
          policy of Never
        reason: ErrImageNeverPull

kubectl debug -it express-d45548897-pwgc6 --image=busybox:1.28 --target express
  - containerID: containerd://a7949e855b9f1c2c52fa76a022377c89628ff27a89c48f673ba8f2041836fa6f
    image: docker.io/library/busybox:1.28
    imageID: docker.io/library/busybox@sha256:141c253bc4c3fd0a201d32dc1f493bcf3fff003b6df416dea4f41046e0f37d47
    lastState: {}
    name: debugger-zhb9v
    ready: false
    restartCount: 0
    state:
      running:
        startedAt: "2023-11-10T13:54:27Z"

kubectl debug -it express-d45548897-km9gj --image=lightruncom/koolkits:jvm --target express
Targeting container "express". If you don't see processes from this container it may be because the container runtime doesn't support this feature.
Defaulting debug container name to debugger-gsmx5.
If you don't see a command prompt, try pressing enter.
root@express-d45548897-km9gj:/usr/local/bin# 
root@express-d45548897-km9gj:/usr/local/bin# curl http://localhost:8080/health-check
Health check passed


kubectl debug -it express-d45548897-km9gj --image=lightruncom/koolkits:jvm --target express
kk express-d45548897-5rpnb jvm express
```